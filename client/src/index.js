import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';

import App from "./modules/app";
import {store, history} from "./modules/app/store";
import {AUTH_USER} from "./modules/auth/actions/types"

const token = localStorage.getItem('token');
if (token) {
    store.dispatch({type: AUTH_USER});
}

render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));
