import merge from 'lodash/merge';
import global from './global';
import dev from './env.development';
import prod from './env.production';

let env = (process.env.REACT_APP_ENV === 'development') ? dev : prod;

export default merge(global, env);