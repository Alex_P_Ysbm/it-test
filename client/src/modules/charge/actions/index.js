import {DO_CHARGE} from "./types";
import request from '../../services/request';
import * as sharedActios from '../../shared/actions'

export function doCharge(amount) {
    return async function (dispatch) {
        try {
            await request({
                method: 'POST',
                url: '/charge',
                data: {amount}
            });
            dispatch({type: DO_CHARGE});
            dispatch(sharedActios.generalNotificationSuccess("Users's credit card have been charged"));
        } catch (e) {
            dispatch(sharedActios.generalNotificationError(e.response.data.errors || e.response.data.message || e.message));
        }
    }
}

