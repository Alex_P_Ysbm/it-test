import React from 'react'
import {Button, Col, FormControl, FormGroup, Glyphicon, InputGroup, Row} from "react-bootstrap"
import * as actions from "../../charge/actions"
import {connect} from "react-redux"
import PropTypes from "prop-types"

class Charge extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            amount: 100
        }
    }

    onAmountChange = (e) => {
        this.setState({
            amount: e.target.value
        });
    }

    onChargeClick = () => {
        this.props.doCharge(this.state.amount);
    }

    render() {
        return (
            <Row>
                <Col mdOffset={6} md={6}>
                    <FormGroup>
                        <InputGroup>
                            <FormControl type="number" value={this.state.amount} onChange={this.onAmountChange}/>
                            <InputGroup.Button>
                                <Button onClick={this.onChargeClick}><Glyphicon glyph="usd"/> Charge users</Button>
                            </InputGroup.Button>
                        </InputGroup>
                    </FormGroup>
                </Col>
            </Row>
        )
    }
}

Charge.propTypes = {
    doCharge: PropTypes.func,
};

export default connect(null, actions)(Charge);
