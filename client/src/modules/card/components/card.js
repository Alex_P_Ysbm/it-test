import React from "react";
import {connect} from "react-redux";
import {Button, ButtonGroup, ButtonToolbar, Col, Glyphicon, Label, Row, Table} from "react-bootstrap";
import * as actions from '../actions';
import {StripeProvider} from "react-stripe-elements"
import EditCard from "./editCard"
import config from "../../../config";
import PropTypes from "prop-types"

class Card extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            isEditCardShow: false
        }
    }

    componentDidMount() {
        this.props.fetchCards();
    }

    onClickDeleteCard(id) {
        this.props.deleteCard(id);
    }

    handleEditCardShow = () => {
        this.setState({
            isEditCardShow: true
        })
    }

    handleCloseEditCard = () => {
        this.setState({
            isEditCardShow: false
        })
    }

    renderItems(cards) {
        return cards.map((item, idx) =>
            (
                <tr key={idx}>
                    <td><h3 className={'mrg-t-0'}><Label bsStyle="info">xxxx xxxx xxxx {item}</Label></h3></td>
                    <td>
                        <ButtonToolbar className={'pull-right'}>
                            <ButtonGroup>
                                <Button className={'btn'} bsStyle="success" bsSize="small" onClick={this.handleEditCardShow}><Glyphicon glyph="edit"/> Edit</Button>
                                <Button className={'btn'} bsStyle="danger" bsSize="small" onClick={() => this.onClickDeleteCard(item)}><Glyphicon glyph="trash"/> Delete</Button>
                            </ButtonGroup>
                        </ButtonToolbar>
                    </td>
                </tr>
            )
        )
    }

    render() {

        const {cards} = this.props;

        return (
            <div>

                    {(cards && cards.length !== 0) || <Row>
                        <Col md={12}>
                            <Button className={'btn pull-right'} bsStyle="primary" bsSize="small" onClick={this.handleEditCardShow}><Glyphicon glyph="plus"/> Add</Button>
                        </Col>
                    </Row>}

                    <Row>
                        <Col smOffset={0} mdOffset={6} sm={12} md={6}>
                            <Table responsive>
                                <thead>
                                <tr>
                                    <th>Last 4</th>
                                    <th className={'text-right'}>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                {cards && this.renderItems(cards)}

                                </tbody>
                            </Table>

                        </Col>
                    </Row>

                <StripeProvider apiKey={config.STRIPE_PUBLIC}>
                    <EditCard isShow={this.state.isEditCardShow} handleClose={this.handleCloseEditCard}/>
                </StripeProvider>
            </div>
        )
    }
}

Card.propTypes = {
    cards: PropTypes.array,
    fetchCards: PropTypes.func,
    deleteCard: PropTypes.func,
};

export default connect((state) => {
    return {
        cards: state.card.cards
    };
}, actions)(Card);
