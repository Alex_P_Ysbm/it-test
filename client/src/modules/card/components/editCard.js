import React from "react";
import {Elements, CardElement, injectStripe} from 'react-stripe-elements';
import {Button, Col, Form, FormGroup, Glyphicon, HelpBlock, Modal} from "react-bootstrap"
import * as actions from '../actions';
import {connect} from "react-redux"
import PropTypes from "prop-types"

class _CardForm extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            error: ''
        }
    }

    setError(message = '') {
        this.setState({
                error: message
            }
        )
    }

    handleFormSubmit = async (e) => {
        e.preventDefault();
        const {stripe, editCard, handleClose} = this.props;
        const payload = await stripe.createToken();
        if (payload.error) {
            this.setError(payload.error.message);
        } else {
            this.setError();
            editCard(payload.token.id);
            handleClose();
        }
    }

    render() {

        return (
            <Form horizontal onSubmit={this.handleFormSubmit}>

                <FormGroup controlId="formHorizontalEmail">
                    <Col sm={12}>
                        <CardElement/>
                    </Col>
                </FormGroup>

                {this.state.error && <HelpBlock className={'danger'}>{this.state.error}</HelpBlock>}

                <FormGroup controlId="formHorizontalEmail">
                    <Col sm={12} className={'text-right'}>
                        <Button type={'submit'} className={'btn'} bsStyle="success" bsSize="small"><Glyphicon glyph="credit-card"/> Save</Button>
                    </Col>
                </FormGroup>
            </Form>

        )
    }
}

_CardForm.propTypes = {
    stripe: PropTypes.object,
    editCard: PropTypes.func,
    handleClose: PropTypes.func,
};

const CardForm = injectStripe(connect(null, actions)(_CardForm));

class EditCard extends React.Component {

    render() {

        const {isShow, handleClose} = this.props;

        return (
            <Modal show={isShow} onHide={handleClose} className="Checkout">
                <Modal.Header closeButton>
                    <Modal.Title>Credit card</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Elements>
                        <CardForm handleClose={handleClose}/>
                    </Elements>
                </Modal.Body>
            </Modal>
        )
    }
}

EditCard.propTypes = {
    isShow: PropTypes.bool,
    handleClose: PropTypes.func,
};

export default EditCard;