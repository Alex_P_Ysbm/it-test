import {FETCH_CARDS, DELETE_CARD, EDIT_CARD} from "./types";
import request from '../../services/request';
import * as sharedActios from '../../shared/actions'

export function fetchCards() {
    return async function (dispatch) {
        try {
            const result = await request({
                method: 'GET',
                url: '/card'
            });
            dispatch({type: FETCH_CARDS, payload: result.data.data});
        } catch (e) {
            dispatch(sharedActios.generalNotificationError(e.response.data.message || e.message));
        }
    }
}

export function deleteCard(id) {
    return async function (dispatch) {
        try {
            await request({
                method: 'DELETE',
                url: `/card`,
                data: {id}
            });
            dispatch({type: DELETE_CARD, payload: id});
        } catch (e) {
            dispatch(sharedActios.generalNotificationError(e.response.data.message || e.message));
        }
    }
}

export function editCard(token) {
    return async function (dispatch) {
        try {
            await request({
                method: 'POST',
                url: '/card',
                data: {token}
            });
            dispatch({type: EDIT_CARD});
            dispatch(fetchCards())
        } catch (e) {
            dispatch(sharedActios.generalNotificationError(e.response.data.message || e.message));
        }
    }
}
