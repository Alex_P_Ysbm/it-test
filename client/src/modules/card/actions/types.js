export const FETCH_CARDS = 'fetch_cards';
export const DELETE_CARD = 'delete_card';
export const EDIT_CARD = 'edit_card';