import React from 'react'
import {Link} from 'react-router-dom'
import {Button, Image, Nav, Navbar, NavItem} from "react-bootstrap"
import {LinkContainer} from "react-router-bootstrap"
import Signin from "../auth/components/signin"
import Signup from "../auth/components/signup"
import {connect} from "react-redux"
import * as authActions from "../auth/actions";
import PropTypes from "prop-types"

class Header extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            isLoginShow: false,
            isSignupShow: false
        }
    }

    handleModalsClose = () => {
        this.setState({
            isLoginShow: false,
            isSignupShow: false
        })
        this.props.authError({});
    }

    handleLoginShow = () => {
        this.setState({
            isLoginShow: true
        })
    }

    handleSignupShow = () => {
        this.props.unsignedup();
        this.setState({
            isSignupShow: true
        })
    }

    static getDerivedStateFromProps(nextProps) {
        if (nextProps.authenticated) {
            return {
                isLoginShow: false,
            }
        }
        if (nextProps.isSignedup) {
            return {
                isSignupShow: false
            }
        }
        return null;
    }

    render() {
        const {authenticated, isRequest} = this.props;
        return (
            <div>
                <Navbar>

                    <Navbar.Header>
                        <Navbar.Brand>
                            <Image src="/images/ajax-loader.gif" className={isRequest || 'invisible'}/>
                        </Navbar.Brand>
                        <Navbar.Brand>
                            <Link to='/'>IT-Rex Test</Link>
                        </Navbar.Brand>
                    </Navbar.Header>

                    <Nav activeKey={1}>
                        <LinkContainer to="/" eventKey={1}>
                            <NavItem>Home</NavItem>
                        </LinkContainer>

                        {authenticated && <LinkContainer to="/card" eventKey={2}>
                            <NavItem>Card</NavItem>
                        </LinkContainer>}
                        {authenticated && <LinkContainer to="/charge" eventKey={2}>
                            <NavItem>Charge</NavItem>
                        </LinkContainer>}

                    </Nav>

                    <Navbar.Form pullRight>
                        {!authenticated && <Button bsStyle="link" onClick={this.handleSignupShow}>Sign Up</Button>}
                        {!authenticated && <Button bsStyle="link" onClick={this.handleLoginShow}>Login</Button>}
                        {authenticated && <Link to="/signout"><Button bsStyle="link">Logout</Button></Link>}
                    </Navbar.Form>

                </Navbar>

                {!authenticated && <Signin isShow={this.state.isLoginShow} handleClose={this.handleModalsClose}/>}
                {!authenticated && <Signup isShow={this.state.isSignupShow} handleClose={this.handleModalsClose}/>}
            </div>
        )
    }
}

Header.propTypes = {
    authError: PropTypes.func,
    unsignedup: PropTypes.func,
    authenticated: PropTypes.bool,
    isRequest: PropTypes.bool,
};

export default connect((state) => {
    return {
        authenticated: state.auth.authenticated,
        isSignedup: state.auth.isSignedup,
        isRequest: state.shared.isRequest
    };
}, authActions)(Header);
