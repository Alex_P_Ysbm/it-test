import {routerMiddleware} from "react-router-redux"
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from "../reducers"
import createHistory from 'history/createBrowserHistory';

const history = createHistory();
const createStoreWithMiddleware = applyMiddleware(reduxThunk, routerMiddleware(history))(createStore);
const store = createStoreWithMiddleware(rootReducer);

export {store, history}