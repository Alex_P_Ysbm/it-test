import React from 'react'
import Header from '../header'
import {Route, Switch} from "react-router-dom"
import Home from "../home"
import Signin from "../auth/components/signin"
import Signout from "../auth/components/signout"
import Card from "../card/components/card"
import Charge from "../charge/components/charge"
import Notifycation from "../shared/components/notifycation"
import checkAuth from "../shared/components/checkAuth"

const App = () => (
    <div>
        <Header/>
        <div className="container">
            <Notifycation/>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route path='/signin' component={Signin}/>
                <Route path='/signout' component={Signout}/>
                <Route path='/card' component={checkAuth(Card)}/>
                <Route path='/charge' component={checkAuth(Charge)}/>
            </Switch>
        </div>
    </div>
)

export default App