import React from 'react'
import {connect} from 'react-redux';
import {Alert} from "react-bootstrap";
import * as actions from "../actions";
import _ from "lodash";
import PropTypes from "prop-types"

class Notifycation extends React.Component {

    // constructor(props, context) {
    //     super(props, context);
    // }

    handleDismiss = () => {
        this.props.dismissNotification();
    }


    renderMessages(messages) {
        return _.map(messages, (message, idx) => {
            return (
                <p key={idx}>{message}</p>
            )
        });
    }

    render() {

        const {notification} = this.props;

        return (
            <div>{
                !_.isEmpty(notification) &&
                <Alert bsStyle={notification.type} onDismiss={this.handleDismiss}>
                    {notification.type === 'danger' && <h4>Oh snap! You got an error!</h4>}
                    {this.renderMessages(notification.messages)}
                </Alert>
            }
            </div>
        )
    }
}

Notifycation.propTypes = {
    notification: PropTypes.object,
    dismissNotification: PropTypes.func,
};

export default connect((state) => {
    return {
        notification: state.shared.notification,
    }
}, actions)(Notifycation)
