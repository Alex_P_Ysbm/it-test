import React, {Component} from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux'
import PropTypes from "prop-types"

function checkAuth(ComposedComponent) {

    class Authentication extends Component {

        componentWillMount() {
            if (!this.props.authenticated) {
                this.props.push('/');
            }
        }

        componentWillUpdate(nextProps) {
            if (!nextProps.authenticated) {
                this.props.push('/');
            }
        }

        render() {
            return <ComposedComponent {...this.props} />
        }
    }

    return connect(
        (state) => {
            return {authenticated: state.auth.authenticated};
        },
        (dispatch) => {
            return {
                push: (path) => {
                    dispatch(push(path));
                }
            }
        }
    )(Authentication);
}

checkAuth.propTypes = {
    authenticated: PropTypes.bool,
    push: PropTypes.func,
};

export default checkAuth;