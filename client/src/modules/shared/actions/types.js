export const GENERAL_NOTIFICATION = 'general_notification';
export const DISMISS_NOTIFICATION = 'dismiss_notification';
export const REQUEST_START = 'request_start';
export const REQUEST_END = 'request_end';