import {GENERAL_NOTIFICATION, DISMISS_NOTIFICATION} from "./types"

export function generalNotificationError(messages) {
    return {
        type: GENERAL_NOTIFICATION,
        payload: {type: 'danger', messages: (typeof messages === 'string' ? {messages} : messages)}

    };
}

export function generalNotificationSuccess(messages) {
    return {
        type: GENERAL_NOTIFICATION,
        payload: {type: 'success', messages: (typeof messages === 'string' ? {messages} : messages)}
    };
}

export function generalNotificationWarning(messages) {
    return {
        type: GENERAL_NOTIFICATION,
        payload: {type: 'warning', messages: (typeof messages === 'string' ? {messages} : messages)}
    };
}

export function dismissNotification() {
    return {
        type: DISMISS_NOTIFICATION,
    };
}
