import React from 'react'
import {connect} from 'react-redux';
import {Alert, Button, Col, ControlLabel, Form, FormGroup, Modal} from "react-bootstrap"
import {Field, reduxForm} from 'redux-form';
import * as actions from '../actions';
import _ from "lodash";
import {reset} from 'redux-form';
import PropTypes from "prop-types"

class Signin extends React.Component {

    // constructor(props, context) {
    //     super(props, context);
    // }

    handleFormSubmit = ({firstName, lastName, email, password}) => {
        this.props.signup({firstName, lastName, email, password});
    }

    renderAlert() {
        const {errors} = this.props;
        return _.map(errors, (error, idx) => {
            return (
                <Alert key={idx} bsStyle="danger">{error}</Alert>
            )
        });
    }

    renderField = ({input, label, type, meta: {touched, error}}) => (
        <FormGroup controlId={input.name}>
            <Col componentClass={ControlLabel} sm={3}>{label}</Col>
            <Col sm={9}>
                <input {...input} placeholder={label} type={type} className="form-control"/>{touched && ((error && <span className={'danger'}>{error}</span>))}
            </Col>
        </FormGroup>
    )

    handleOnEnter = () => {
        this.props.resetForm();
    }

    render() {

        const {isShow, handleSubmit, submitting, handleClose} = this.props;

        return (
            <Modal show={isShow} onHide={handleClose} onEnter={this.handleOnEnter}>
                <Modal.Header closeButton>
                    <Modal.Title>Sign Up</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit)}>

                        <Field name="firstName" type="text" component={this.renderField} label="First name"/>
                        <Field name="lastName" type="text" component={this.renderField} label="Last name"/>
                        <Field name="email" type="email" component={this.renderField} label="Email"/>
                        <Field name="password" type="password" component={this.renderField} label="Password"/>

                        <FormGroup>
                            <Col smOffset={3} sm={9}>
                                {this.renderAlert()}
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={3} sm={9}>
                                <Button type="submit" disabled={submitting} className={'pull-right'}>Sign Up</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}

const validate = values => {
    const errors = {}
    if (!values.firstName) {
        errors.firstName = 'First name is required'
    }
    if (!values.lastName) {
        errors.lastName = 'Last name is required'
    }
    if (!values.password) {
        errors.password = 'Password is required'
    }
    if (!values.email) {
        errors.email = 'Email is required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }
    return errors
}

Signin.propTypes = {
    signup: PropTypes.func,
    resetForm: PropTypes.func,
    errors: PropTypes.object,
    isShow: PropTypes.bool,
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    handleClose: PropTypes.func
};

export default reduxForm({form: 'signup', validate, destroyOnUnmount: false})(connect(
    (state) => {
        return {errors: state.auth.errors}
    },
    (dispatch) => {
        return {
            signup: (user) => dispatch(actions.signup(user)),
            resetForm: () => dispatch(reset('signup'))
        }
    }
)(Signin));

