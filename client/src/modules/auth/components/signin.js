import React from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Alert, Button, Col, ControlLabel, Form, FormGroup, Modal} from "react-bootstrap"
import {Field, reduxForm} from 'redux-form';
import * as actions from '../actions';
import _ from "lodash";

class Signin extends React.Component {

    // constructor(props, context) {
    //     super(props, context);
    // }

    handleFormSubmit = ({email, password}) => {
        this.props.signin({email, password});
    }

    renderAlert() {
        const {errors} = this.props;
        return _.map(errors, (error, idx) => {
            return (
                <Alert key={idx} bsStyle="danger">{error}</Alert>
            )
        });
    }

    render() {

        const {isShow, handleClose, handleSubmit} = this.props;

        return (
            <Modal show={isShow} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form horizontal onSubmit={handleSubmit(this.handleFormSubmit)}>

                        <FormGroup controlId="formHorizontalEmail">
                            <Col componentClass={ControlLabel} sm={3}>Email</Col>
                            <Col sm={9}>
                                <Field name="email" component="input" type="email" placeholder="Email" className="form-control"/>
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalPassword">
                            <Col componentClass={ControlLabel} sm={3}>Password</Col>
                            <Col sm={9}>
                                <Field name="password" component="input" type="password" placeholder="Password" className="form-control"/>
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={3} sm={9}>
                                {this.renderAlert()}
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={3} sm={9}>
                                <Button type="submit" className={'pull-right'}>Sign in</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}

Signin.propTypes = {
    signin: PropTypes.func,
    handleClose: PropTypes.func,
    handleSubmit: PropTypes.func,
    errors: PropTypes.object,
    isShow: PropTypes.bool,
};

export default reduxForm({form: 'signin'})(connect(
    (state) => {
        return {errors: state.auth.errors}
    },
    actions
)(Signin));

