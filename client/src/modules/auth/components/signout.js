import React, {Component} from 'react';
import {connect} from 'react-redux';
import {signout} from '../actions';
import * as sharedActions from '../../shared/actions';
import {push} from "react-router-redux"
import PropTypes from "prop-types"

class Signout extends Component {

    componentWillMount() {
        this.props.signout();
        this.props.generalNotificationWarning('Logging out...')
        setTimeout(() => {
            this.props.dismissNotification()
            this.props.push('/');
        }, 2000);
    }

    render() {
        return (
            <div></div>
        )
    }
}

Signout.propTypes = {
    signout: PropTypes.func,
    generalNotificationWarning: PropTypes.func,
    dismissNotification: PropTypes.func,
    push: PropTypes.func,
};

export default connect(null, (dispatch) => {
    return {
        signout: () => dispatch(signout()),
        generalNotificationWarning: (message) => dispatch(sharedActions.generalNotificationWarning(message)),
        dismissNotification: () => dispatch(sharedActions.dismissNotification()),
        push: (path) => {
            dispatch(push(path));
        }
    }
})(Signout)
