import {AUTH_ERRORS, AUTH_USER, UNAUTH_USER, SIGNEDUP_USER, UNSIGNEDUP_USER} from "./types";
import {push} from 'react-router-redux';
import request from "../../services/request"
import * as sharedActions from "../../shared/actions";

export function signin({email, password}) {
    return async function (dispatch) {
        try {
            const result = await request({
                method: 'POST',
                url: `/token`,
                data: {email, password}
            });
            dispatch({type: AUTH_USER});
            localStorage.setItem('token', result.data.data);
            dispatch(push('/'))
        } catch (e) {
            dispatch(authError(e.response.data.message || e.message));
        }
    }
}

export function signup(user) {
    return async function (dispatch) {
        try {
            await request({
                method: 'POST',
                url: `/users`,
                data: user
            });

            console.log('signed')

            dispatch({type: SIGNEDUP_USER});
            dispatch(sharedActions.generalNotificationSuccess('Your registration has been successful. Please login.'));
        } catch (e) {
            dispatch(authError(e.response.data.errors || e.response.data.message || e.message));
        }
    }
}

export function signout() {
    localStorage.removeItem('token');
    return {
        type: UNAUTH_USER
    };
}

export function unsignedup() {
    return {
        type: UNSIGNEDUP_USER
    };
}

export function authError(errors) {
    return {
        type: AUTH_ERRORS,
        payload: (typeof errors === 'string' ? {errors} : errors)
    };
}
