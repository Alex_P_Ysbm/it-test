export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const SIGNEDUP_USER = 'signedup_user';
export const UNSIGNEDUP_USER = 'unsignedup_user';
export const AUTH_ERRORS = 'auth_errors';
