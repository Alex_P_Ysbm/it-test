import axios from 'axios';
import config from "../../config";
import {store} from "../app/store";
import {REQUEST_END, REQUEST_START} from "../shared/actions/types";

const client = axios.create({
    baseURL: config.API_URL,
});

/**
 * Defaults
 */
client.defaults.timeout = config.TIMEOUT;

const request = async (options) => {
    try {
        options.headers = {...options.headers, 'Authorization': localStorage.getItem('token') || ''};
        store.dispatch({type: REQUEST_START});
        const result = await client(options);
        store.dispatch({type: REQUEST_END});
        return result;
    } catch (e) {
        if (e.response) {
            console.error('Status:', e.response.status);
            console.error('Data:', e.response.data);
        } else {
            console.error('Error Message:', e.message);
        }
        store.dispatch({type: REQUEST_END});
        throw e;
    }
}

export default request;