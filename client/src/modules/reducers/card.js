import {FETCH_CARDS, DELETE_CARD, EDIT_CARD} from "../card/actions/types";

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_CARDS :
            return {...state, cards: action.payload}
        case DELETE_CARD :
            return {...state, cards: state.cards.filter(card => action.payload !== card)}
        case EDIT_CARD :
            return state
        default:
            return state;
    }
};