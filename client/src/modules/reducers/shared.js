import {GENERAL_NOTIFICATION, DISMISS_NOTIFICATION, REQUEST_START, REQUEST_END} from "../shared/actions/types";

export default (state = {notification: {}, isRequest: false}, action) => {
    switch (action.type) {
        case GENERAL_NOTIFICATION :
            return {...state, notification: action.payload}
        case DISMISS_NOTIFICATION :
            return {...state, notification: {}}
        case REQUEST_START :
            return {...state, isRequest: true}
        case REQUEST_END :
            return {...state, isRequest: false}
        default:
            return state;
    }
};