import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import auth from './auth';
import card from './card';
import shared from './shared';
import {routerReducer} from 'react-router-redux'

const rootReducer = combineReducers({
    form,
    auth,
    card,
    shared,
    router: routerReducer
});

export default rootReducer;
