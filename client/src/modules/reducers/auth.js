import {AUTH_ERRORS, AUTH_USER, UNAUTH_USER, SIGNEDUP_USER, UNSIGNEDUP_USER} from "../auth/actions/types";

export default (state = {errors: {}, authenticated: false, isSignedup: false}, action) => {
    switch (action.type) {
        case AUTH_USER :
            return {...state, errors: {}, authenticated: true};
        case UNAUTH_USER :
            return {...state, authenticated: false};
        case SIGNEDUP_USER :
            return {...state, errors: {}, isSignedup: true};
        case UNSIGNEDUP_USER :
            return {...state, errors: {}, isSignedup: false};
        case AUTH_ERRORS:
            return {...state, errors: action.payload};
        default:
            return state;
    }
};