const { User } = require('../models/user');
const jwtService = require('../services/jwt');

async function createToken(ctx) {
  const { email, password } = ctx.request.body;

  if (!(email && password)) {
    ctx.throw(400, 'Email and password are required');
  }

  const user = await User.findOne({ email });

  if (!user) {
    ctx.throw(400, 'User not found');
  }

  if (!user.comparePasswords(password)) {
    ctx.throw(400, 'Please check your credentials');
  }

  const token = await jwtService.genToken({ id: user.id });

  ctx.body = { data: token };
  ctx.status = 201;
}

module.exports = { createToken };
