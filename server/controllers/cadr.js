const { Card, cardHandler } = require('../models/card');
const _ = require('lodash');
const stripe = require('../services/stripe');

/**
 * Attach card to auth user
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function attachCard(ctx) {
  const { user } = ctx.state;

  /**
     * Create Card object
     */
  const card = new Card(_.pick(ctx.request.body, Card.createFields));

  /**
     * Validate
     */
  await cardHandler.validate(card);

  /**
     * Attach to the user
     */
  const serviceCard = await stripe.updateCard(user.serviceId, card.token);

  ctx.body = { data: serviceCard.last4 };
  ctx.status = 200;
}

/**
 * Attach card to auth user
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function getCard(ctx) {
  const { user } = ctx.state;

  const serviceCard = await stripe.getCard(user.serviceId);
  let data = serviceCard.last4 ? [serviceCard.last4] : [];

  ctx.body = { data };
  ctx.status = 200;
}

/**
 * Delete user's card
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function deleteCard(ctx) {
  const { user } = ctx.state;

  await stripe.deleteCard(user.serviceId);

  ctx.body = { data: 'ok' };
  ctx.status = 200;
}

module.exports = { attachCard, getCard, deleteCard };
