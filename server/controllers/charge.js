const _ = require('lodash');
const { userHandler } = require('../models/user');
const stripe = require('../services/stripe');
const moment = require('moment');
const config = require('../config').app;
const email = require('../helpers/email');
const { agenda, jobs } = require('../helpers/agenda');

/**
 * Mass charge
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function createMassCharge(ctx) {
  const { amount } = _.pick(ctx.request.body, 'amount');

  /**
     * Validation
     */
  if (!_.toSafeInteger(amount)) {
    ctx.throw({
      errors: {
        amount:
               {
                 message: 'Amount is required and should be numeric',
                 path: 'amount',
               },
      },
      name: 'ValidationError',
    });
  }

  /**
     * Get charge list for the last 'config.daysToSkipCharge' days
     */
  const chargeList = await stripe.getCharges(moment().subtract(config.daysToSkipCharge, 'days').unix(), moment().unix());

  (await userHandler.find({})).map(async (user) => {
    if (!user.serviceId || _.isEmpty(await stripe.getCard(user.serviceId))) {
      /**
          *  If no credit card in user record during charge, then we need to delete this user
          */
      if (user.serviceId) {
        await stripe.deleteCustomer(user.serviceId);
      }
      user.remove();
    } else if (!_.find(chargeList, { customer: user.serviceId })) {
    /**
             * When we call the charge end-point, then if previous transaction
             * failed or completed less then 7 days ago,
             * then we shouldn't try to charge this user again.
             */
      try {
        await stripe.chargeCustomer(user.serviceId, amount);
      } catch (e) {
      /**
                 * If transaction failed, then we need to send
                 * email to user and try to charge this user again after 24 hours.
                 */
        email.sendEmail({
          to: user.email,
          subject: 'IT test',
          text: 'Your payment was declined',
        });

        agenda.schedule('24 hours', jobs.CHARGE, { _id: user.id, amount });
      }
    }
  });

  ctx.body = { data: 'ok' };
  ctx.status = 200;
}


module.exports = { createMassCharge };
