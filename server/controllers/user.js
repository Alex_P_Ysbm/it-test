const { userHandler, User } = require('../models/user');
const _ = require('lodash');
const stripe = require('../services/stripe');

module.exports = {

  /**
     * Create new user
     *
     * @param ctx
     * @returns {Promise<void>}
     */
  async createUser(ctx) {
    const userData = _.pick(ctx.request.body, User.createFields);

    /**
         * Create new system user
         */
    const newUser = await userHandler.create(userData);

    /**
         * Create new service user
         */
    const customer = await stripe.createCustomer(newUser.email);

    /**
         * Update system user with service id
         */
    userHandler.update(newUser, { serviceId: customer.id });

    const user = await userHandler.findOne({ _id: newUser.id });
    ctx.body = { data: user };
    ctx.status = 201;
  },

  /**
     * Get authenticated user
     *
     * @param ctx
     * @returns {Promise<void>}
     */
  async getAuthUser(ctx) {
    const { state: { user: { _id } } } = ctx;
    const user = await userHandler.findOne({ _id });
    ctx.body = { data: user };
    ctx.status = 200;
  },
};
