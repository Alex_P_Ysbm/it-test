/**
 * 3th
 */
const Koa = require('koa');

/**
 * 1th
 */
const config = require('./config');
const DB = require('./db');
const middlewares = require('./middlewares');
const routes = require('./routes');
const logger = require('./helpers/logger');

/**
 * App & DB
 */
const app = new Koa();
DB.initConnection();

/**
 * Middlewares
 */
middlewares(app);

/**
 * Routes
 */
app.use(routes.api.routes());
app.use(routes.api.allowedMethods());
app.use(routes.web.routes());
app.use(routes.web.allowedMethods());

const server = app.listen(config.system.port, (err) => {
  if (err) logger.error(err);
  logger.info(`Listening on http://localhost:${config.system.port}`);
});

module.exports = server;
