const jwt = require('jsonwebtoken');
const config = require('../config').system;

module.exports = {

  /**
     * Generate token
     *
     * @param data
     * @returns {*}
     */
  genToken(data) {
    return jwt.sign(data, config.key);
  },

  /**
     * Verify token
     *
     * @param token
     * @returns {*}
     */
  verify(token) {
    return jwt.verify(token, config.key);
  },
};
