const config = require('../config');
const stripe = require('stripe')(config.stripe.secret_key);

function retrieveDefaultCard(customer) {
  return customer.default_source ?
    customer.sources.data.find(item => item.id === customer.default_source)
    : {};
}

module.exports = {

  /**
     * Create a new customer
     *
     * @param email
     * @returns {Promise<*>}
     */
  async createCustomer(email) {
    return stripe.customers.create({ email });
  },

  /**
     * Delete customer
     *
     * @param customerId
     * @returns {Promise<*>}
     */
  async deleteCustomer(customerId) {
    return stripe.customers.del(customerId);
  },

  /**
     * Attach a card to the customer
     *
     * @param customerId
     * @param cardToken
     */
  async attachCard(customerId, cardToken) {
    return retrieveDefaultCard(await stripe.customers.createSource(customerId, {
      source: cardToken,
    }));
  },

  /**
     * Update default customer's card
     *
     * @param customerId
     * @param cardToken
     * @returns {Promise<*>}
     */
  async updateCard(customerId, cardToken) {
    return retrieveDefaultCard(await stripe.customers.update(customerId, {
      source: cardToken,
    }));
  },

  /**
     * Delete customer's card
     *
     * @param customerId
     * @returns {Promise<*>}
     */
  async deleteCard(customerId) {
    const sources = await stripe.customers.listSources(customerId);
    sources.data.forEach(async (card) => {
      await stripe.customers.deleteSource(customerId, card.id);
    });
    return true;
  },

  /**
     * Get customer's card
     *
     * @param customerId
     * @returns {Promise<*>}
     */
  async getCard(customerId) {
    return retrieveDefaultCard(await stripe.customers.retrieve(customerId));
  },

  /**
     * Charging an customer
     *
     * @param customer
     * @param amount
     * @param currency
     * @returns {Promise<*>}
     */
  async chargeCustomer(customer, amount, currency = 'usd') {
    return stripe.charges.create({
      amount: (amount * 100),
      currency,
      customer,
    });
  },

  /**
     * Get charges list
     *
     * @param gte - date start
     * @param lte - date end
     * @returns {Promise<void>}
     */
  async getCharges(gte, lte) {
    const list = await stripe.charges.list({
      created: { gte, lte },
    });
    return list.data;
  },

};
