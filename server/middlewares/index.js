const config = require('../config');
const koaLogger = require('koa-logger');
const logger = require('../helpers/logger');
const cors = require('@koa/cors');
const serveStatic = require('koa-better-static2');
const bodyParser = require('koa-bodyparser');
const favicon = require('koa-favicon');
const error = require('./error');
const auth = require('./auth');
const render = require('koa-ejs');
const path = require('path');

const CLIENT_PATH = path.join(__dirname, '..', '..', 'client', 'public');

module.exports = (app) => {
  if (config.env === 'development') {
    app.use(koaLogger(logger.stream));
  }
  app.use(error());
  app.use(cors());
  app.use(favicon(`${CLIENT_PATH}/images/favicon.ico`));
  app.use(serveStatic(CLIENT_PATH, { maxage: 0 }));
  app.use(bodyParser());
  app.use(auth.retrieveAuth);

  render(app, {
    root: CLIENT_PATH,
    layout: false,
    viewExt: 'html',
    cache: false,
    debug: (config.env === 'development'),
  });
};
