const jwtService = require('../services/jwt');
const { User } = require('../models/user');

module.exports = {
  async retrieveAuth(ctx, next) {
    const { authorization } = ctx.headers;
    if (authorization) {
      try {
        const { id } = await jwtService.verify(authorization);
        ctx.state.user = await User.findById(id);
      } catch (e) {
        ctx.throw(401, 'Invalid Token');
      }
    }
    await next();
  },
  async checkAuth(ctx, next) {
    if (!ctx.state.user) {
      ctx.throw(403, 'Forbidden');
    }
    await next();
  },
};
