const config = require('../config');
const logger = require('../helpers/logger');

module.exports = () => async (ctx, next) => {
  try {
    await next();
  } catch (e) {
    const {
      status = 500, message = 'Server Error', name, errors,
    } = e;
    if (name === 'ValidationError') {
      ctx.status = 400;
      ctx.body = {
        errors: Object.values(errors).reduce((errs, er) => ({
          ...errs,
          [er.path]: er.message,
        }), 0),
      };
    } else {
      ctx.status = status;
      ctx.body = { status, message };
    }
    if (config.env === 'development') {
      logger.debug(e);
    }
    logger.error(`${status} ${ctx.method} ${ctx.href} | ${message} | ${ctx.ip}`);
  }
};
