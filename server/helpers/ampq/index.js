const amqp = require('amqplib');
const config = require('../../config').messaging;
const logger = require('../logger');

const rabbitMqUrl = ['amqp://', config.rabbitMq.user, ':', config.rabbitMq.password, '@', config.rabbitMq.host].join('');

/**
 * Consume message
 *
 * @param handler
 */
function consumer(handler) {
  function getQueueHandler(channel) {
    return function queueHandler(msg) {
      (async () => {
        try {
          const body = JSON.parse(msg.content.toString());
          if (await handler(body)) {
            channel.ack(msg);
          } else {
            channel.nack(msg, false, false);
          }
        } catch (e) {
          channel.nack(msg, false, false);
          logger.error(e);
        }
      })();
    };
  }

  amqp.connect(rabbitMqUrl)
    .then((connection) => {
      process.once('SIGINT', () => {
        connection.close();
      });

      return connection.createChannel()
        .then((channel) => {
          let ok = channel.assertQueue(config.queue, config.rabbitMq.options || {
            durable: true,
          });
          ok = ok.then(() => {
            channel.prefetch(1);
          });
          ok = ok.then(() => {
            channel.consume(config.queue, getQueueHandler(channel), { noAck: false });
          });
          return ok;
        });
    }).then(null, logger.warning);
}

/**
 * Publish a message
 *
 * @param queue
 * @param data
 */
function publish(queue, data) {
  amqp.connect(rabbitMqUrl)
    .then((connection) => {
      process.once('SIGINT', () => {
        connection.close();
      });

      return connection.createChannel()
        .then((channel) => {
          const ok = channel.assertQueue(queue, config.rabbitMq.options || {
            durable: true,
          });
          return ok.then(() => {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            return channel.close();
          });
        });
    }).then(null, logger.warning);
}

module.exports = { consumer, publish };
