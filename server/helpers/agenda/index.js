const Agenda = require('agenda');
const mongoose = require('mongoose');
const jobs = require('./jobs');

module.exports = {
  agenda: new Agenda({
    mongo: mongoose.connection,
    processEvery: '10 seconds',
  }),
  jobs,
};
