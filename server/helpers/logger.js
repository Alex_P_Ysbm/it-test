const winston = require('winston');
const config = require('../config').winston;
const fs = require('fs');
const path = require('path');

const logDirectory = path.dirname(config.file.filename);

if (!fs.existsSync(logDirectory)) {
  fs.mkdirSync(logDirectory);
}

const logger = new winston.Logger({
  transports: [
    new winston.transports.File(config.file),
    new winston.transports.Console(config.console),
  ],
  exitOnError: false,
});

logger.stream = (message) => {
  logger.info(message);
};

module.exports = logger;
