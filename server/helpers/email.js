const { publish } = require('./ampq');
const queues = require('./ampq/queues');

async function sendEmail(data) {
  publish(queues.EMAIL, data);
}

module.exports = { sendEmail };
