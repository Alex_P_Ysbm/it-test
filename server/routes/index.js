const Router = require('koa-router');
const userController = require('../controllers/user');
const tokenController = require('../controllers/token');
const cardController = require('../controllers/cadr');
const chargeController = require('../controllers/charge');
const auth = require('../middlewares/auth');

const api = new Router({
  prefix: '/api/v1/',
});

api
  .get('who-am-i', auth.checkAuth, userController.getAuthUser)
  .post('token', tokenController.createToken)
  .post('users', userController.createUser)
  .get('card', auth.checkAuth, cardController.getCard)
  .post('card', auth.checkAuth, cardController.attachCard)
  .put('card', auth.checkAuth, cardController.attachCard)
  .delete('card', auth.checkAuth, cardController.deleteCard)
  .post('charge', chargeController.createMassCharge);

const web = new Router();

web.get('/', async (ctx) => {
  await ctx.render('index');
});

module.exports = { api, web };
