const config = require('../config').schedule;
const { agenda } = require('../helpers/agenda');
const DB = require('../db');
const logger = require('../helpers/logger');

config.jobs.forEach((job) => {
// eslint-disable-next-line import/no-dynamic-require,global-require
  require(`./jobs/${job}`)(agenda);
});

if (config.jobs.length) {
  DB.initConnection();
  agenda.on('ready', () => {
    agenda.start();
    logger.info('Scheduler started');
  });
} else {
  logger.error('Scheduler: no jobs are specified.');
}
