const { userHandler } = require('../../models/user');
const { jobs } = require('../../helpers/agenda');
const stripe = require('../../services/stripe');
const logger = require('../../helpers/logger');

module.exports = (agenda) => {
  agenda.define(jobs.CHARGE, async (job, done) => {
    const { _id, amount } = job.attrs.data;
    const user = await userHandler.findOne({ _id });
    if (user) {
      try {
        await stripe.chargeCustomer(user.serviceId, amount);
        logger.info(`The user has been charged: ${user.email} ${amount}`);
      } catch (e) {
        /**
           * If after 24 hours second charge will failed again, then we need to delete this user.
           */
        if (user.serviceId) {
          await stripe.deleteCustomer(user.serviceId);
        }
        user.remove();
        logger.info(`The user has been removed: ${user.email}`);
      }
    } else {
      logger.error(`Scheduler: Charge: Could not find user: ${_id}`);
    }
    done();
  });
};
