const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema(
  {
    token: {
      type: String,
      required: 'Token is required',
      trim: true,
    },
  },
  {
    id: false,
    timestamps: true,
    toJSON: {
      versionKey: false,
    },
  },
);

schema.statics.createFields = ['token'];

module.exports = mongoose.model('Card', schema);
