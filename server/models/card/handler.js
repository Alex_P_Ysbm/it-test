const AppError = require('../../helpers/appError');

module.exports =
    {
      async validate(card) {
        try {
          return await card.validate();
        } catch (e) {
          throw new AppError({ status: 400, ...e });
        }
      },
    };
