const Card = require('./model');
const cardHandler = require('./handler');

module.exports = {
  Card,
  cardHandler,
};
