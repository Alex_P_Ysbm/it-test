const User = require('./model');
const userHandler = require('./handler');

module.exports = {
  User,
  userHandler,
};
