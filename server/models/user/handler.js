const AppError = require('../../helpers/appError');
const User = require('./model');
const { Card } = require('../card');

module.exports =
    {
      async create(data) {
        try {
          return await User.create(data);
        } catch (e) {
          throw new AppError({ status: 400, ...e });
        }
      },
      async update(user, data) {
        try {
          user.set(data);
          return await user.save();
        } catch (e) {
          throw new AppError({ status: 400, ...e });
        }
      },
      async findOne(params) {
        return User.findOne(params).select(User.selectFields).populate({ path: 'card', select: Card.selectFields });
      },
      async find(params) {
        return User.find(params).select(User.selectFields);
      },
    };
