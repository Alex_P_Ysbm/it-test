const mongoose = require('mongoose');

const { Schema } = mongoose;
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');

const schema = new Schema(
  {
    email: {
      type: String,
      unique: 'User with email {VALUE} already exist',
      lowercase: true,
      required: 'Email is required',
      trim: true,
    },
    password: {
      type: String,
      required: 'Password is required',
      trim: true,
    },
    firstName: {
      type: String,
      lowercase: true,
      required: 'First name is required',
      trim: true,
    },
    lastName: {
      type: String,
      lowercase: true,
      required: 'Last name is required',
      trim: true,
    },
    serviceId: {
      type: String,
    },
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
      versionKey: false,
    },
  },
);

schema.plugin(uniqueValidator);

schema.statics.createFields = ['email', 'firstName', 'lastName', 'password'];

schema.statics.selectFields = { password: 0, createdAt: 0, updatedAt: 0 };

schema.virtual('card', {
  ref: 'Card',
  localField: '_id',
  foreignField: 'user',
  justOne: true,
});

function preSave(next) {
  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, salt);
  }
  next();
}

schema.pre('save', preSave);

schema.methods = {
  comparePasswords(password) {
    return bcrypt.compareSync(password, this.password);
  },
};


module.exports = mongoose.model('User', schema);
