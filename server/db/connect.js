const mongoose = require('mongoose');
const logger = require('../helpers/logger');

mongoose.Promise = Promise;

module.exports = (uri) => {
  if (!uri) {
    throw Error('Database URI is undefined');
  }
  return mongoose
    .connect(uri)
    .then((mongodb) => {
      logger.info('DB connected');
      return mongodb;
    });
};
