const connect = require('./connect');
const config = require('../config').db;
const logger = require('../helpers/logger');

module.exports = {
  async initConnection() {
    try {
      await connect(config.url);
    } catch (e) {
      logger.error(e);
    }
  },
};
