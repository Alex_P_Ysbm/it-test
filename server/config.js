const nconf = require('nconf');
const path = require('path');

nconf
  .argv()
  .env({
    transform(object) {
      const obj = object;
      if (typeof obj.value !== 'string') {
        return false;
      }
      obj.value = obj.value.trim();
      return obj;
    },
  })
  .file({ file: path.join(__dirname, '..', '/env.local.json') })
  .defaults({
    env: nconf.get('NODE_ENV') || 'development',
    system: {
      port: nconf.get('NODE_PORT') || 3001,
    },
    app: {
      daysToSkipCharge: 7,
    },
    messaging: {
      worker: process.env.NODE_WORKER,
      queue: process.env.NODE_QUEUE,
    },
    schedule: {
      jobs: (process.env.NODE_JOBS ? process.env.NODE_JOBS.split(',') : []),
    },
    winston: {
      file: {
        level: 'error',
        filename: path.join(__dirname, '..', '/logs/app.log'),
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
      },
      console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
      },
    },
  });

module.exports = nconf.get();
