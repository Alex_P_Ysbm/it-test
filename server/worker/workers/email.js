const { consumer } = require('../../helpers/ampq');
const nodemailer = require('nodemailer');
const config = require('../../config').email;
const logger = require('../../helpers/logger');

const emailTransport = nodemailer.createTransport(config.nodemailer, config.defaults);

async function handler(data) {
  logger.info(`Processing message for: ${data.to} subject: ${data.subject}`);
  try {
    await emailTransport.sendMail(data);
    return true;
  } catch (e) {
    logger.error(e);
    return false;
  }
}

consumer(handler);
